var skEventHandling = function(event){
    this.event = event;
};

skEventHandling.prototype.on = function(targets) {
    var typof = typeof targets;
    if(typof == "string") {
        this.targetsText = targets;
        this.targets = document.querySelectorAll(targets);
    } else if(targets instanceof Element) {
        this.targets = [targets];
    } else if(targets instanceof NodeList) {
        this.targets = targets;
    } else {
        throw "No se paso un selector o elemento válido";
    }

    return this;
};

skEventHandling.prototype.into = function(parent) {
    var typof = typeof targetContainers;
    if(typof == "string") {
        this.targetContainers = document.querySelectorAll(targetContainers);
    } else if(targetContainers instanceof Element) {
        this.targetContainers = targetContainers;
    } else if(targetContainers instanceof NodeList) {
        throw "No se admiten listas de nodos";
    } else {
        throw "No se paso un selector o elemento válido";
    }

    return this;
};

skEventHandling.prototype.do = function(handler){
    var self = this;

    this.handler = handler;

    if(!this.targetContainers || !this.targetsText) {
        [].forEach.call(this.targets, function(target) {
            target.addEventListener(self.event, self.handler);
        });
    } else {
        [].forEach.call(this.targetContainers, function(targetContainer) {
            targetContainer.addEventListener(self.event, function(e) {
                var target = e.target;
                if(target.matches(self.targetsText) && self.isDescendantOf(target, targetContainer)) {
                    self.handler.call(target, e);
                }
            });
        });
    }

    return this;
};

skEventHandling.prototype.isDescendantOf = function(child, suposedParent) {
    var node = child.parentNode;
     while (node != null) {
         if (node == suposedParent) {
             return true;
         }
         node = node.parentNode;
     }
     return false;
}



Storage.prototype.setj = function(clave, valor){
    return localStorage.setItem(clave, JSON.stringify(valor));
}
Storage.prototype.getj = function(claves, options){
    options || (options = {});
    if(typeof claves == "object" && claves.length) {
        sk.http('GET', this.source || '/lsto', {items:claves}, function(data){
            sk.oFor(data, function(id, values){
                sk.lsto.setj(id, values);
            });
        }, false);
        options.success == "function" && options.success(ret);
    } else {
        var item = localStorage.getItem(claves);
        if(!item) {
            sk.http('GET', this.source || '/lsto', {items:claves}, function(data){
                sk.oFor(data, function(id, values){
                    sk.lsto.setj(id, values);
                });
            }, false);
            item = localStorage.getItem(claves);
        }
        var ret = this.magic(JSON.parse(item));
        if(options.volatile) localStorage.removeItem(claves);
        typeof options.success == "function" && options.success(ret);
        return ret;
    }
}
Storage.prototype.magic = function(obj) {
    if(typeof obj != "undefined"){
        if( typeof obj.length == "undefined" ) { // un unico elemento
            obj.select = function(search){
                var pattern = /\{\{\s*([^}]+)\s*\}\}/g;
                var matches = [];
                while( match = pattern.exec(search) )
                    matches.push(match);
                for(var i = 0; i < matches.length; i++)
                {
                    var ar = matches[i][1].split('.');
                    var original = this[ar[0].trim()];
                    for(var x = 1; x < ar.length; x++)
                        original = eval("original."+ar[x]);
                    search = search.replace(matches[i][0], original);
                }
                return search;
            }
        } else { // lista de elementos
            obj.first = function(){
                return sk.lsto.magic(this[0]);
            }
            obj.where = function(clave, valor, busqueda_exacta){
                if(typeof valor == "undefined")
                    return sk.lsto.magic([]);
                return sk.lsto.magic(this.filter(function(obj){
                    if(obj[clave] != void 0){
                       if(typeof busqueda_exacta == "undefined" || busqueda_exacta == false)
                           return obj[clave].toString().toLowerCase().indexOf(valor.toString().toLowerCase()) > -1;
                       else
                           return obj[clave].toString().toLowerCase() == valor.toString().toLowerCase();
                    } else {
                        return false;
                    }
                }));
            }
            obj.whereIn = function(clave, valores, busqueda_exacta){
                return localStorage.magic(this.filter(function(obj){
                    var encontrado = false;
                    for(var i = 0; i < valores.length; i++ ){
                        if(obj[clave] != void 0) {
                            if(typeof busqueda_exacta == "undefined" || busqueda_exacta == false){
                                encontrado = obj[clave].toString().toLowerCase().indexOf(valores[i].toString().toLowerCase()) > -1;
                            } else {
                                encontrado = obj[clave].toString().toLowerCase() == valores[i].toString().toLowerCase();
                            }
                            if(encontrado) return true;
                        }
                    }
                    return false;
                }));
            }
            obj.whereNotIn = function(clave, valores, busqueda_exacta){
                return localStorage.magic(this.filter(function(obj){
                    var encontrado = false;
                    for(var i = 0; i < valores.length; i++ ){
                        if(typeof busqueda_exacta == "undefined" || busqueda_exacta == false)
                            encontrado = obj[clave].toString().toLowerCase().indexOf(valores[i].toString().toLowerCase()) > -1;
                        else
                            encontrado = obj[clave].toString().toLowerCase() == valores[i].toString().toLowerCase();
                        if(encontrado)
                            return false;
                    }
                    return true;
                }));
            }
            obj.each = function(callback){
                for(var x=0; x<obj.length; x++)
                    callback(localStorage.magic(obj[x]), x);
                return obj;
            }
            obj.toArray = function(key, value){
                out = {}
                for (var i = 0; i < obj.length; i++) {
                    out[obj[i][key]] = obj[i][value]
                };
                return out;
            }
        }
    }

    return obj;
}





/* VALIDATOR */
var skValidator = function(patterns) {
    this.error_types = {
        date: "El campo %s no es una fecha válida (%s)",
        email: "El campo %s debe ser una cuenta de email válida",
        alpha: "El campo %s debe contener solo caracteres",
        numeric: "El campo %s no es un número",
        decimal: "El campo %s no es un número decimal",
        alphanumeric: "El campo %s debe ser alfanumérico",
        minLen: "El campo %s debe tener al menos %s caracteres de longitud",
        maxLen: "El campo %s no debe superar los %s caracteres de longitud",
        betweenLen: "El campo %s debe tener entre %s y %s caracteres",
        length: "El campo %s debe tener %s caracteres", 
        required: "El campo %s es requerido",
    };
    this.errores = {};
    this.field_validation = {};
    this.patterns = patterns;
}

skValidator.prototype.do_validation = function(key, value){
    if(key in this.patterns) {
        var validations = this.patterns[key].split("|"),
            length = validations.length,
            i=0, v, a1, a2;

        for(; i<length; i++){
            v = validations[i];

            switch(v){
                case 'email':
                    if(!this.email(value)) this.throw('email', [key]); break;
                case 'alpha':
                    if(!this.alpha(value)) this.throw('alpha', [key]); break;
                case 'numeric':
                    if(!this.numeric(value)) this.throw('numeric', [key]); break;
                case 'decimal':
                    if(!this.decimal(value)) this.throw('decimal', [key]); break;
                case 'alphanumeric':
                    if(!this.alphanumeric(value)) this.throw('alphanumeric', [key]); break;
                case 'required':
                    if(!this.required(value)) this.throw('required', [key]); break;
                default:
                    if(v.indexOf("minLen") === 0) {
                        a1 = v.split(":")[1];
                        if(!this.minLen(value, a1)) this.throw('minLen', [key, a1]);
                    } else if(v.indexOf("maxLen") === 0) {
                        a1 = v.split(":")[1];
                        if(!this.maxLen(value, a1)) this.throw('maxLen', [key, a1]);
                    } else if(v.indexOf("betweenLen") === 0) {
                        [undefined, a1, a2] = /betweenLen:(\d+),(\d+)/.exec(v);
                        if(!this.betweenLen(value, a1, a2)) this.throw('betweenLen', [key, a1, a2]);
                    } else if(v.indexOf("length") === 0) {
                        a1 = v.split(":")[1];
                        if(!this.length(value, a1)) this.throw('length', [key, a1]);
                    } else if(v.indexOf("date") === 0) {
                        a1 = v.split(":")[1];
                        if(!this.date(value, a1)) this.throw('date', [key, a1]);
                    }
                    break;
            }
        }
    }
}

skValidator.prototype.validate = function(data){
    var validator = this;


    this.clean();    
    sk.oFor(data, function(k, v) {
        validator.do_validation(k, v);
        validator.field_validation[k] = this;
    });

    return !this.hasErrors();
}

skValidator.prototype.date = function(date, format){
    format || (format = 'dd/mm/yyyy');
    format = '^'+format.replace(/[dmy]/g, '\\d').replace(/\//g, '\\/')+'$';

    var reg = new RegExp(format);
    return reg.test(date);
}

skValidator.prototype.email = function(email){
    var reg = new RegExp(/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/);
    return reg.test(email);
}

skValidator.prototype.alpha = function(str){
    var reg = new RegExp(/^[a-zA-Z\s\ñ\Ñ]*$/);
    return reg.test(str);
}

skValidator.prototype.numeric = function(number){
    if(number!==0 && !number) return true;
    var reg = new RegExp(/^[0-9]*$/);
    return reg.test(number)
}

skValidator.prototype.decimal = function(number) {
    return !isNaN(number);
}

skValidator.prototype.alphanumeric = function(str){
    var reg = new RegExp(/[0-9a-zA-Z\s\ñ\Ñ]*/);
    return reg.test(str);
}

skValidator.prototype.minLen = function(str, len){
    if(typeof str != "string") return false;
    if(str.trim()=="") return true;
    return str.length>=len;
}

skValidator.prototype.maxLen = function(str, len){
    if(typeof str != "string") return false;
    if(str.trim()=="") return true;
    return str.length<=len;
}

skValidator.prototype.betweenLen = function(str, min, max){
    return this.maxLen(str, max) && this.minLen(str, min);
}

skValidator.prototype.length = function(str, len){
    return this.betweenLen(str, len, len);
}

skValidator.prototype.required = function(str){
    if(typeof str == "string")
        return str.trim().length!=0;
    if(str == void 0 || str===null)
        return false;

    return true;
}

skValidator.prototype.clean = function(){
    this.errores={};
    this.field_validation={};

    return this;
}

skValidator.prototype.get = function(field) {
    if(!field) return this.errores;

    if(field in this.errores)
        return this.errores[field]
    return [];
}

skValidator.prototype.hasErrors = function(){
    return Object.keys(this.errores).length > 0;
}

skValidator.prototype.throw = function(error_type, data) {
    var err = this.error_types[error_type];
    var field = data[0];
    this.errores[field] || (this.errores[field]=[]);

    for(var i = 0; i < data.length; i++)
        err = err.replace("%s", data[i]);

    this.errores[field].push(err);
}

skValidator.prototype.throwCustom = function(field, err){
    this.errores[field] || (this.errores[field]=[]);
    this.errores[field].push(err);
}

skValidator.prototype.paint = function(clear_on_change, errores){
    var validator = this;
    errores || (errores = this.errores);

    for(var field in errores){
        (function(field){
            var validation = validator.field_validation[field];
            var input = document.querySelector("#"+field+",[name="+field+"]")

            if(validation.plugin && validation.plugin=="select2"){ // input-border for select2
                input = input.parentNode.querySelector('#s2id_'+input.id+' a.select2-choice');
            }

            if(!("border" in input.dataset))
                input.dataset.border = input.style.border || '';
            input.style.cssText = "border:1px solid #fd3535 !important";

            if(clear_on_change===true) {
                if(validation.plugin && validation.plugin=="select2") {
                    var jq = validation.jquery || $;
                    jq(validation.selector).on('change', function(){  input.style.border = input.dataset.border;  });
                } else {
                    input.addEventListener('input', function(){  this.style.border = input.dataset.border;  });
                }
            }
        })(field);
    }
}

/* LITTLE FORM */
var skLittleForm = function(opts){

    if (!(this instanceof skLittleForm)){
        var obj = Object.create(skLittleForm.prototype);
        return skLittleForm.apply(obj, arguments);
    }


    this.options = {
        method: 'POST',
        fields: [],
        title: ''
    };

    sk.extend(this.options, opts);
    var fields      = this.options.fields,
        l           = fields.length,
        i           = 0,
        container   = document.createElement("div"),
        header      = document.createElement("div"),
        body      = document.createElement("div"),
        title      = document.createElement("div"),
        closeBtn    = document.createElement("i"),
        el, f, k;

    closeBtn.innerHTML = "&#10006;";
    title.innerText = this.options.title;

    container.classList.add("lf-container");
    header.classList.add("header");
    title.classList.add("title")
    closeBtn.classList.add("close-btn");
    body.classList.add("body");

    header.appendChild(title);
    header.appendChild(closeBtn);
    container.appendChild(header);



    for (; i < l; i++) {
        f = fields[i];
        el = document.createElement(f.node);
        delete f.node;
        for(k in f) {
            el[k] = f[k];
        }

        body.appendChild(el);
    }

    container.appendChild(body);

    this.main = container;

    return this;

    // document.body.appendChild(container);
};

skLittleForm.prototype.show = function(effect) {
    this.main.classList.add(effect);
    document.body.appendChild(this.main);
    setTimeout(function(){
        this.main.classList.add('active');
        this.main.querySelector("input").focus();
    }.bind(this), 1);

    return this;
};

skLittleForm.prototype.hide = function() {
    this.main.classList.remove('active');
};



var swikni = sk = {
    /* gSerialize */
    serialize: function(container, options){
        var wrapper = (typeof container == "string") ? document.querySelector(container) : container;
        var selector = 'input,textarea,select';
        var c = wrapper.querySelectorAll(selector),
            l = c.length,
            out = {},
            i = 0;

        checkInputs:
        for (; i < l; i++) {
            var e = c[i], value;
            /*fix select2
            var classList = e.classList;
            for(var x = 0; x < classList.length; x++)
                if(classList[x].indexOf('select2')!=-1 && classList[x]!='select2-offscreen')
                    continue checkInputs;
            */

            //continue normally
            var s = e.id || e.name;
            if(!s) continue;
            if(e.type=="checkbox") value = e.checked;
            else
            {
                if(e.type=="select-multiple")
                {
                    var options = e.selectedOptions;
                    value=[];
                    for(var x=0; x<options.length; x++)
                        value.push(options[x].value);
                }
                else
                    value = e.value;
            }
            out[s] = value;
        }
        if(typeof options == "object") {
            for(var opt in options) {
                switch(opt){
                    case 'drop_empties':
                        if(options[opt])
                            for(var k in out)
                                if(typeof out[k] == "string" && out[k].trim()=="")
                                    delete out[k];
                        break;
                    case 'bool_to_int':
                        if(options[opt])
                            for(var k in out)
                                if(typeof out[k] == "boolean")
                                    out[k] = (out[k]) ? 1 : 0;
                        break;
                }
            }
        }
        return out;
    },


    /* VALIDATOR */
    validator: skValidator,


    /* LSTO */
    lsto: localStorage,


    /* AJAX */
    dataBody: 'POST|PUT|DELETE',

    xhrSetup: {},

    http: function(method, url, data, callback, async){
        (async !== void 0) || (async = true);
        var xhr = new XMLHttpRequest();
        if(method=="GET" && Object.keys(data).length) {
            var new_data = [];
            sk.oFor(data, function(k, v){
                if(typeof v == "object" && v.length) {
                    var ek = encodeURIComponent(k)+'[]';
                    for (var i = 0; i < v.length; i++) new_data.push( ek+'='+encodeURIComponent(v[i]) );
                } else {
                    new_data.push( encodeURIComponent(k)+'='+encodeURIComponent(v) );
                }
            });
            url += '?'+new_data.join('&');
        }
        xhr.open(method, url, async);
        xhr.setRequestHeader('Accept', 'application/json');
        xhr.setRequestHeader('Content-Type', 'application/json');
        this.oFor(this.xhrSetup.headers, function(k, v){
            xhr.setRequestHeader(k, v);
        });
        xhr.onload = function(e) {
            callback(JSON.parse(this.response));
        }
        xhr.send( sk.dataBody.indexOf(method.toUpperCase())!=-1?JSON.stringify(data):'' );

        return xhr;
    },


    /* HELPERS */
    events: {
        change: new UIEvent("change", {
            "view": window,
            "bubbles": true,
            "cancelable": true
        })
    },

    observers: {},
    stopObserver: function(key) {
        if(key in this.observers)
            return this.observers[key].stop();
    },

    enable: function(e, recursive){
        typeof e == "string" && (e=document.qs(e));
        this.changeState(e, false);
        if(recursive) {
            [].forEach.call(e.children, function(child){
                sk.enable(child, true);
            })
        }
    },

    disable: function(e, recursive) {
        typeof e == "string" && (e=document.qs(e));
        this.changeState(e, true);
        if(recursive) {
            [].forEach.call(e.children, function(child){
                sk.disable(child, true);
            })
        }
    },

    changeState: function(e, state){
        var el;
        if("currentTarget" in e) {
            if(e.currentTarget) el = e.currentTarget;
            else if(e.target) el = e.target;
        } else {
            var el = e;
        }
        if(el.nodeName == "A") {
            el.style.pointerEvents = (state)?"none":'';
        } else {
            el.disabled = state;
        }
    },

    isEmpty: function(obj){
        return (obj.length != void 0) ? obj.length==0 : Object.keys(obj).length==0;
    },

    parent: function(selector){
        var found=false, current=this, func;
        ("matches" in this && (func="matches")) || ("matchesSelector" in this && (func="matchesSelector")) || 
        ("webkitMatchesSelector" in this && (func="webkitMatchesSelector")) || ("mozMatchesSelector" in this && (func="mozMatchesSelector")) || ("msMatchesSelector" in this && (func="msMatchesSelector")) || ("oMatchesSelector" in this && (func="oMatchesSelector"));

        if(func == void 0) return;

        do {
            current = current.parentNode;
            found = eval('current.'+func+'(selector)');
        } while(!found);

        return current;
    },

    nullsForAjax: function(obj){
        if(typeof obj == "object" && Object.keys(obj).length) for(var k in obj) if(obj[k]===null) obj[k] = undefined;
        return obj;
    },

    buildUrl: function(sections){
        return sections.join('/');
    },

    when: function(event){
        return new skEventHandling(event);
    },

    addEventListener: function(event, cb){
        if(this.length != void(0)){
            [].forEach.call(this, function(el){
                el.addEventListener(event, cb);
            });
        } else {
            this.addEventListener(event, cb);
        }
    },

    $addEventListener: function(event, cb){
        $(this).on(event, cb);
    },

    fillSelect: function(selector, placeholder, collection, key, value){
        key || (key="id");
        value || (value="nombre");

        var select = typeof(selector)=="string" ? document.querySelector(selector) : selector;
        var options = '<option value="">'+placeholder+'</option>';
        var option_template = '<option value="%s">%s</option>';

        collection.forEach(function(opt){
            options += option_template.replace('%s', opt[key]).replace('%s', opt[value]);
        });

        select.innerHTML = options;
    },

    copy: function(data){
        this.oFor(data, function(k, v){
            var to = document.qs(v);
            to.value = document.qs(k).value;
            to.dispatchEvent(sk.events.change);
        });
    },

    oFor: function(o, cb){
        for(var k in o) cb.call(o, k, o[k]);
    },

    oFilter: function(i, cb){
        var o = {};
        for(var k in i) if(cb(k, i[k])) o[k]=i[k];
        return o;
    },

    select: function(action){
        [].forEach.call(arguments, function(arg){
            var el = document.querySelector(arg);
            if(el) {
                switch(action){
                    case 'empty':
                        el.innerHTML = "";
                        break;
                    case 'clean':
                        el.value="";
                        el.dispatchEvent(sk.events.change);
                        break;
                }
            }
        });
    },

    select2: function(action){
        var jq;
        if(typeof arguments[1] == "function") jq = [].shift.call(arguments);
        else jq = $;

        [].forEach.call(arguments, function(arg){
            var el = document.querySelector(arg);
            if(el){
                switch(action){
                    case 'clean':
                        jq(el).val('').trigger('change');
                        break;
                    case 'empty':
                        jq(el).html('');
                        break;
                }
            }
        });
    },

    option: function(value, text){
        return '<option value="'+value+'">'+text+'</option>';
    },

    extend: function(){
        var src = arguments[0],i=0,l=arguments.length,k;
        for (; i < l; i++)
            for(k in arguments[i])
                src[k] = arguments[i][k];
    },

    completeForm: function(form, data) {
        typeof form == "string" && (form = document.qs(form));
        sk.oFor(data, function(key, value) {
            if(typeof value != "object") {
                var el = form.qs("#"+key);
                if(el) {
                    if(el.nodeName == "INPUT") {
                        if(el.type == "text") {
                            el.value = value;
                        }
                    } else if(el.nodeName == "SELECT") {
                        var option = el.qs('[value="'+value+'"]');
                        if(option) {
                            el.selectedIndex = option.index;
                            el.dispatchEvent(sk.events.change);
                        }
                    }
                }
            }
        });
    },

    elementToSelector: function(element, withId, depth) {
        withId === void 0 && (withId = false);
        if(depth === 0) {
            return;   
        } else if(isNaN(depth)) {
            depth = -1;
        }
        var selector = "";
        if(element) {
            if(element == document.body) return 'body';
            var id = element.id ? "#"+element.id : "";
            var clases = element.classList.length ? "."+[].join.call(element.classList, ".") : "";

            selector = arguments.callee(element.parentNode, withId, depth - 1) + " " + element.nodeName.toLowerCase() + (withId && id?id:'') + clases;
        } 

        return selector.trim();
    },

    append: function(container, str) {
        var outsiders = (function(str){
            var i,
                len = str.length,
                c,
                insideElementContent = 0,
                insideElementDeclaration,
                insideQuotes,
                insideText,
                insideCloseTag,
                start = 0,
                out = [];

            for (i = 0; i < len; i++) {
                c = str[i];
                switch (c) {
                    case '"':
                        insideElementDeclaration && (insideQuotes = !insideQuotes);
                        break
                    case '<':
                        if (insideText) {
                            insideElementDeclaration = true;
                            insideText = false;
                            out.push(str.substring(start, i));
                            start = i;
                        } else if (insideElementContent) {
                            if (str[i + 1] == "/") {
                                insideCloseTag = true;
                            } else {
                                insideElementDeclaration = true;
                            }
                        } else {
                            insideElementDeclaration = true;
                        }
                        break;
                    case '>':
                        if (insideElementDeclaration && !insideQuotes) {
                            insideElementDeclaration = false;
                            insideElementContent++;
                        } else if (insideCloseTag) {
                            insideCloseTag = false;
                            insideElementContent--;
                            if (!insideElementContent) {
                                out.push(str.substring(start, i+1));
                                start = i+1;
                            }
                        }
                        break;
                    default:
                        if (!insideText && !insideElementContent && !insideElementDeclaration && !insideQuotes && !insideCloseTag) {
                            insideText = true;
                        }
                        break;
                }
            }
            
            if(i!=start) out.push(str.substring(start, i));

            return out;
        })(str);

        var regexHTML = /\s*<\w+.*?>/,
            regexTag = /<\/?\w+((\s+\w+(\s*=\s*(?:".*?"|'.*?'|[\^'">\s]+))?)+\s*|\s*)\/?>/,
            regexNode = /\s*<(\w+)/,
            regexAttr = /(\w+)\s*=\s*"(.*?)"/g,
            regexInner = /<(\w+)((\s+\w+(\s*=\s*(?:".*?"|'.*?'|[\^'">\s]+))?)+\s*|\s*)\/?>(.*?)<\/\1>/;
                
        var nodes=[],
            tag,
            node,
            match,
            tempNode,
            inner;

        outsiders.forEach(function(outsider){
            if(regexHTML.test(outsider)) {
                tag = outsider.match(regexTag)[0];
                node = regexNode.exec(tag)[1];
                tempNode = document.createElement(node);
                while((match = regexAttr.exec(tag)) !== null) {
                    tempNode.setAttribute(match[1], match[2]);
                }
                inner = regexInner.exec(outsider);
                if(inner && 5 in inner)
                    tempNode.innerHTML = inner[5];
                nodes.push(tempNode);
            } else {
                nodes.push(document.createTextNode(outsider));
            }
        });

        nodes.forEach(function(n){
            container.appendChild(n);
        });

    },

    littleForm: skLittleForm,
}

/* PROTOTYPES */
HTMLElement.prototype.parent = sk.parent;
HTMLElement.prototype.$addEventListener = sk.$addEventListener;
NodeList.prototype.addEventListener = sk.addEventListener;
NodeList.prototype.$addEventListener = sk.$addEventListener;
Element.prototype.qs = Element.prototype.querySelector;
Element.prototype.qsa = Element.prototype.querySelectorAll;
HTMLDocument.prototype.id = HTMLDocument.prototype.getElementById;
HTMLDocument.prototype.qs = HTMLDocument.prototype.querySelector;
HTMLDocument.prototype.qsa = HTMLDocument.prototype.querySelectorAll;
var byId = function(id) {return document.id(id)}



